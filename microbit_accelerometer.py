from microbit import *
import math

def main():
    IMG_ACCEL = Image("00900:09990:00900:00900:00900")
    IMG_DECEL = Image("00900:00900:00900:09990:00900")
    IMG_NEUTRAL = Image("00000:00000:09990:00000:00000")
    IMG_STOP = Image("09990:90009:99999:90009:09990")

    TRESHOLD_POS = 100
    TRESHOLD_NEG = -100
    HARSH_STOP = 250

    BEEP_FOR_MS = 3000
    SAMPLING_TIME_MS = 100

    while True:
        x = accelerometer.get_x()
        y = accelerometer.get_y()
        z = accelerometer.get_z()

        accel = math.sqrt(x**2 + y**2 + z**2)
        print("({}, {})".format(z, accel))

        if z >= TRESHOLD_NEG and z <= TRESHOLD_POS:
            display.show(IMG_NEUTRAL)
        else:
            if z < 0:
                display.show(IMG_ACCEL)
            else:
                pin0.write_digital(1)
                if z > HARSH_STOP:
                    display.show(IMG_STOP)
                    sleep(BEEP_FOR_MS)
                else:
                    display.show(IMG_DECEL)

        sleep(SAMPLING_TIME_MS)
        display.clear()
        pin0.write_digital(0)

main()